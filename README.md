# PipeScore

A bagpipe notation software currently under development. Uses the Express NodeJS backend and Pug for rendering templates, p5 for drawing the score and p5.pdf for converting to PDF.

You can view the site-in-progress at https://pipescore.herokuapp.com.